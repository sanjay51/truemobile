<?php
  include_once "comparator.php";

  function getImageLink($postID, $index) {
    $endpoint = "https://s3.ap-south-1.amazonaws.com";
    $folder = "phoneimages/phone/post_" . $postID . "/800x800";
    $image = $index . ".jpg";
    $link = $endpoint . "/" . $folder . "/" . $image;

    return $link;
  }

  function getMetaImageLink($key, $ext = 'jpg') {
    $endpoint = "https://s3.ap-south-1.amazonaws.com";
    $folder = "phoneimages/sitemeta";
    $image = $key . "." . $ext;
    $link = $endpoint . "/" . $folder . "/" . $image;

    return $link;
  }

  function getAllPhones() {
    $args = array(); //no filter => select all
    $submissions = Ninja_Forms()->subs()->get( $args );

    return $submissions;
  }

  function getPhoneByPostID($postID) {
    $args = array(
      'fields'    => array(
        RelatedPostID   => $postID
      )
    );

    $submissions = Ninja_Forms()->subs()->get( $args );
    $first_form_element = reset($submissions);
    return new Mobile($first_form_element);
  }

  function convertSubmissionsToMobiles($submissions) {
    $mobiles = array();
    foreach ($submissions as $sub) {
      array_push($mobiles, new Mobile($sub));
    }

    return $mobiles;
  }

  function getTopTen($mobiles) {
    usort($mobiles, "compareMobiles");
    return $mobiles;
  }

  function compareMobiles($mobile1, $mobile2) {
    static $comparator = null;

    if ($comparator == null) {
      $comparator = new Comparator();
    }

    return $comparator->compareMobile($mobile1, $mobile2);
  }
?>
