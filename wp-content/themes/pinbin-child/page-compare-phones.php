<?php
/**
* Page compare: Single page template
*/
include_once "utils.php";
include_once "mobile.php";
include_once "constants.php";
?>

<?php get_header(); ?>
<!-- page content starts here -->
<?php
$phoneID1 = $_GET['pid1'];
$phoneID2 = $_GET['pid2'];
$arePhonesLoaded = false;

if (isset($phoneID1) && isset($phoneID2)) {
  $phone1 = getPhoneByPostID($phoneID1);
  $phone2 = getPhoneByPostID($phoneID2);
	$arePhonesLoaded = true;
}

?>

<div class="container" style="background-color:white; box-shadow:0px 0px 10px black; padding:10px">

  <?php include ('phone_view/compare/row_select.php'); ?>

	<?php
	if ($arePhonesLoaded) {
		?>

		<!-- TODO: Make table responsive -->
		<table class="table">

			<tr>
				<th></th>
				<th></th>
				<th><?= $phone1->get(Title); ?></th>
				<th><?= $phone2->get(Title); ?></th>
			</tr>

			<tr>
				<td>Price</td>
				<td></td>
				<td>₹<?= $phone1->get(PriceINR); ?>
					<a target='_blank' href="<?= $phone1->get(SellerLink) ?>">
						<button type="button" class="btn btn-hot btn-success btn-xs">buy from <?= $phone1->get(Seller) ?></button>
					</a>
				</td>
				<td>₹<?= $phone2->get(PriceINR); ?>
					<a target='_blank' href="<?= $phone2->get(SellerLink) ?>">
						<button type="button" class="btn btn-hot btn-xs">buy from <?= $phone2->get(Seller) ?></button>
					</a>
				</td>
			</tr>

			<tr>
				<td></br/>Display</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

			<tr>
				<td></td>
				<td>Screen size (inches)</td>
				<td><?= $phone1->get(ScreenSizeInches); ?>"</td>
				<td><?= $phone2->get(ScreenSizeInches); ?>"</td>
			</tr>

			<tr>
				<td></td>
				<td>Screen type</td>
				<td><?= $phone1->getDisplayType() ?></td>
				<td><?= $phone2->getDisplayType() ?></td>
			</tr>

			<tr>
				<td></td>
				<td>Pixel density</td>
				<td><?= $phone1->get(ScreenPixel) . " (" . $phone1->get(ScreenPpi) . " ppi)"; ?></td>
				<td><?= $phone2->get(ScreenPixel) . " (" . $phone1->get(ScreenPpi) . " ppi)"; ?></td>
			</tr>

			<tr>
				<td></td>
				<td>Screen glass</td>
				<td><?= $phone1->get(ScreenGlassType); ?></td>
				<td><?= $phone2->get(ScreenGlassType); ?></td>
			</tr>

			<tr>
				<td></br/>Performance</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

			<tr>
				<td></td>
				<td>RAM</td>
				<td><?= $phone1->getRAMInGB(); ?> GB</td>
				<td><?= $phone2->getRAMInGB(); ?> GB</td>
			</tr>

			<tr>
				<td></td>
				<td>Processor</td>
				<td><?= $phone1->get(ProcessorCores) . "-core " . $phone1->get(ProcessorMake); ?></td>
				<td><?= $phone2->get(ProcessorCores) . "-core " . $phone2->get(ProcessorMake); ?></td>
			</tr>

			<tr>
				<td></td>
				<td>Processor speed</td>
				<td><?= $phone1->get(ProcessorGHz); ?> GHz</td>
				<td><?= $phone2->get(ProcessorGHz); ?> GHz</td>
			</tr>

			<tr>
				<td></td>
				<td>Antutu score</td>
				<td><?= $phone1->get(AntutuScore); ?></td>
				<td><?= $phone2->get(AntutuScore); ?></td>
			</tr>

			<tr>
				<td></td>
				<td>Internal storage</td>
				<td><?= $phone1->get(StorageInternalGB);?> GB</td>
				<td><?= $phone2->get(StorageInternalGB);?> GB</td>
			</tr>

			<tr>
				<td></td>
				<td>External storage</td>
				<td><?= $phone1->getExternalStorage(); ?></td>
				<td><?= $phone2->getExternalStorage(); ?></td>
			</tr>

			<tr>
				<td></br/>Battery</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

			<tr>
				<td></td>
				<td>Type</td>
				<td><?= $phone1->get(BatteryType); ?></td>
				<td><?= $phone2->get(BatteryType); ?></td>
			</tr>

			<tr>
				<td></td>
				<td>Capacity</td>
				<td><?= $phone1->get(BatterymAH); ?></td>
				<td><?= $phone2->get(BatterymAH); ?></td>
			</tr>

			<tr>
				<td></td>
				<td>FastCharge</td>
				<td><?= $phone1->get(FastCharge); ?></td>
				<td><?= $phone2->get(FastCharge); ?></td>
			</tr>

			<tr>
				<td></td>
				<td>Charge type</td>
				<td><?= $phone1->get(ChargingType); ?></td>
				<td><?= $phone2->get(ChargingType); ?></td>
			</tr>

			<tr>
				<td><br/>Camera</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

			<tr>
				<td></td>
				<td>Rear</td>
				<td><?= $phone1->get(CameraRearMP); ?> MP</td>
				<td><?= $phone2->get(CameraRearMP); ?> MP</td>
			</tr>

			<tr>
				<td></td>
				<td>Front</td>
				<td><?= $phone1->get(CameraFrontMP); ?> MP</td>
				<td><?= $phone2->get(CameraFrontMP); ?> MP</td>
			</tr>

			<tr>
				<td><br/>Other features</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

			<tr>
				<td></td>
				<td>FingerPrint</td>
				<td><?= $phone1->get(FingerPrint); ?></td>
				<td><?= $phone2->get(FingerPrint); ?></td>
			</tr>

			<tr>
				<td></td>
				<td>Network</td>
				<td><?= $phone1->get(Network); ?> MP</td>
				<td><?= $phone2->get(Network); ?> MP</td>
			</tr>

			<tr>
				<td></td>
				<td>Weight</td>
				<td><?= $phone1->get(WeightGrams); ?>g</td>
				<td><?= $phone2->get(WeightGrams); ?>g</td>
			</tr>

			<tr>
				<td></td>
				<td>OS</td>
				<td>Android <?= $phone1->get(AndroidVersion); ?></td>
				<td>Android <?= $phone2->get(AndroidVersion); ?></td>
			</tr>

		</table>
		<?php
	}
	?>
</div>

<!-- page content ends here -->

<?php get_footer(); ?>
