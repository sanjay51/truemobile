<?php
define ('RelatedPostID', '44');
define ('PhoneCompany', '37');
define ('PhoneModel', '38');
define ('Title', '40');
define ('PriceINR', '34');

define ('ScreenSizeInches', '7');
define ('ScreenType', '8');
define ('ScreenPixel', '9');
define ('ScreenPpi', '10');
define ('ScreenGlassType', '11');
define ('ScreenExtras', '12');

define ('RAMinMB', '13');
define ('StorageInternalGB', '14');
define ('StorageExternalGB', '15');

define ('ProcessorMake', '16');
define ('ProcessorCores', '17');
define ('ProcessorGHz', '18');

define ('BatteryType', '19');
define ('BatterymAH', '20');

define ('CameraRearMP', '22');
define ('CameraFrontMP', '23');
define ('CameraExtras', '24');

define ('AndroidVersion', '25');

define ('SIM1Type', '26');
define ('SIM1Size', '27');
define ('SIM2Type', '28');
define ('SIM2Size', '43');

define ('FingerPrint', '29');
define ('ChargingType', '30');
define ('FastCharge', 'Yes');
define ('Network', '31');
define ('WeightGrams', '32');
define ('OtherFeatures', '33');
define ('Seller', '35');
define ('SellerLink', '36');
define ('AntutuScore', '39');
define ('ReviewLinkHindi', '41');
define ('ReviewLinkEnglish', '43');
?>
