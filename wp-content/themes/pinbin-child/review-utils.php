<?php
$primary_rev_template = "
[Name] is a [ScreenSizeInches]\" [phoneType] phone that comes with [displayType] display and [ScreenPixel] resolution.<br/>
</br/>
[Name] is a [performance] phone. The company [Company] has chosen [ProcessorMake] processor, which comes with [ProcessorCores] cores of [ProcessorGHz] GHz each, a fairly strong choice for this price segment, especially considering alternate phones which are available at much higher price. It's packed with [RAMinGB] GB of RAM and [StorageInternalGB] GB of storage, which is not expandable.
";

$sec_rev_template = "
The smartphone has a [simType] card tray with both slots supporting 2G, 3G and 4G. Power is supplied by a [BatterymAH] mAh, non-removable battery with a [ChargingType] charger. <br/>
</br/>[Name] runs the in-house developed EUI v. 5.6.014 based on Android [AndroidVersion]. It also boast of a fingerprint sensor. Network bands supported are [Network], and one-handed usage is fairly easy thanks to the narrow sides and weight of [WeightGrams]g.
";

function getPrimaryReviewTemplate($mobile) {
  global $primary_rev_template;
  $template = $primary_rev_template;

  return replacePlaceholders($template, $mobile);
}

function getSecondaryReviewTemplate($mobile) {
  global $sec_rev_template;
  $template = $sec_rev_template;

  return replacePlaceholders($template, $mobile);
}

function replacePlaceholders($text, $mobile) {
  $phone_name = $mobile->get(PhoneCompany) . " " . $mobile->get(PhoneModel);

  $text = str_replace("[Name]", $phone_name, $text);
  $text = str_replace("[ScreenSizeInches]", $mobile->get(ScreenSizeInches), $text);
  $text = str_replace("[phoneType]", $mobile->getPhoneType(), $text);
  $text = str_replace("[displayType]", $mobile->getDisplayType(), $text);
  $text = str_replace("[ScreenPixel]", $mobile->get(ScreenPixel), $text);
  $text = str_replace("[performance]", "high performance", $text);
  $text = str_replace("[Company]", $mobile->get(PhoneCompany), $text);
  $text = str_replace("[ProcessorMake]", $mobile->get(ProcessorMake), $text);
  $text = str_replace("[ProcessorCores]", $mobile->get(ProcessorCores), $text);
  $text = str_replace("[ProcessorGHz]", $mobile->get(ProcessorGHz), $text);
  $text = str_replace("[RAMinGB]", $mobile->getRAMInGB(), $text);
  $text = str_replace("[StorageInternalGB]", $mobile->get(StorageInternalGB), $text);
  $text = str_replace("[Network]", $mobile->get(Network), $text);

  $simType = "single SIM";
  if ($mobile->get(SIM2Type) != "") {
    $simType = "dual-SIM";
  }
  $text = str_replace("[simType]", $simType, $text);

  $text = str_replace("[BatterymAH]", $mobile->get(BatterymAH), $text);
  $text = str_replace("[ChargingType]", $mobile->get(ChargingType), $text);
  $text = str_replace("[AndroidVersion]", $mobile->get(AndroidVersion), $text);
  $text = str_replace("[Network]", $mobile->get(BatterymAH), $text);
  $text = str_replace("[WeightGrams]", $mobile->get(WeightGrams), $text);

  return $text;
}
?>
