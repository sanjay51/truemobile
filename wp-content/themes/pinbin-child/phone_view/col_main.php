<div class="col-sm-4">
  <h1 style="border-bottom:5px solid #34a6ed; color: #33adea  ; text-align:center; padding: 10px;"><b><?= $mobile->get(Title) ?></b>
  </h1>
  <br/>
  <h3 style="text-align:right">₹ <?= $mobile->get(PriceINR) ?>
    <small>
      <a target='_blank' href="<?= $mobile->get(SellerLink) ?>">
        <button style="color:#33adea;" type="button" class="btn btn-primary">buy from <?= $mobile->get(Seller) ?></button>
      </a>
    </small>
  </h3>

  <ul style="text-align:right; width:100%; list-style:none; margin-top:10%">
    <li class="fact">#1 in <a href="http://androidjunglee.com/best-android-phones-under-15000.html">Best phones under 15000</a></li>
    <li class="fact">One of the <a href="http://bestgadgetry.com/5-best-smartphones-under-rs-15000-in-india/">best phones under 20000</a></li>
    <li class="fact"><?= $mobile->get(AntutuScore) ?><u> Antutu score</u></li>
  </ul>
</div>
