<!-- Start of primary specs -->
<div class="row" style="margin-left:10%; margin-right: 10%; margin-top:5%">
  <div class="col-sm-6">
    <div class="panel panel-primary" style="margin:5px;" >
      <div class="panel-body">

        <!-- Display -->
        <?php
        $screenType = $mobile->get(ScreenType);
        if ($screenType == "fullhd") {
          $screenType = "Full HD";
        }

        $display = $mobile->get(ScreenSizeInches) . "\" ";
        $display .= $screenType . ", ";
        $display .= $mobile->get(ScreenPixel) . " pixels ";
        $display .= "(" . $mobile->get(ScreenPpi) . " ppi)";
        ?>
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('display', 'png') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Display</b></div>
              <div class="panel-body"><?= $display ?></div>
            </div>
          </div>
        </div>

        <!-- RAM -->
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('ram2') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>RAM</b></div>
              <div class="panel-body"><?= $mobile->get(RAMinMB)/1024 ?> GB</div>
            </div>
          </div>
        </div>

        <!-- Storage -->
        <?php
        $rom = $mobile->get(StorageInternalGB) . " GB internal<br/>";
        $external = $mobile->get(StorageExternalGB);
        if ($external == '0') {
          $external = 'External memory card not supported';
        } else {
          $external .= " GB external (memory card supported)";
        }
        $rom .= $external;
        ?>
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('rom') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Storage</b></div>
              <div class="panel-body"><?= $rom ?></div>
            </div>
          </div>
        </div>

        <!-- Processor -->
        <?php
        $cores = $mobile->get(ProcessorCores);
        $make = $mobile->get(ProcessorMake);
        $ghz = $mobile->get(ProcessorGHz);
        if ($cores == '8') {
          $cores = 'Octa';
        }
        $processor = $ghz . " GHz " . $cores . "-core " . $make;
        ?>
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('processor') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Processor</b></div>
              <div class="panel-body"><?= $processor ?></div>
            </div>
          </div>
        </div>

        <!-- Battery -->
        <?php
        $mah = $mobile->get(BatterymAH);
        $batteryType = $mobile->get(BatteryType);
        ?>
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('battery') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Battery</b></div>
              <div class="panel-body"><?= $mah . " mAH " .  $batteryType ?></div>
            </div>
          </div>
        </div>

        <!-- Camera -->
        <?php
        $camRearMP = $mobile->get(CameraRearMP);
        $camFrontMP = $mobile->get(CameraFrontMP);
        $camExtras = $mobile->get(CameraExtras);
        $cam = $camRearMP . " MP rear<br/>";
        $cam .= $camFrontMP . " MP front";
        ?>
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('camera', 'png') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Camera</b></div>
              <div class="panel-body"><?= $cam ?><br/><small><?= $camExtras ?></small></div>
            </div>
          </div>
        </div>

        <!-- End of primary specs -->
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <?= "<h4>" . $mobile->getPrimaryReview() . "</h4>" ?>
  </div>
</div>
