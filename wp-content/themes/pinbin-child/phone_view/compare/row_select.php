<?php
$phones = getAllPhones();
?>
<script>
function xyz() {
  var selectPhone1 = document.getElementById("selectPhone1");
  var phoneID1 = selectPhone1.options[selectPhone1.selectedIndex].value;
  var phoneTitle1 = selectPhone1.options[selectPhone1.selectedIndex].innerHTML;

  var selectPhone2 = document.getElementById("selectPhone2");
  var phoneID2 = selectPhone2.options[selectPhone2.selectedIndex].value;
  var phoneTitle2 = selectPhone2.options[selectPhone2.selectedIndex].innerHTML;

  var newlocation = "compare-phones?compare_" + phoneTitle1 + " vs " + phoneTitle2;
  var params = "pid1=" + phoneID1 + "&pid2=" + phoneID2;
  window.location = newlocation + "&" + params;
}
</script>

<div class="row">
  <div class="col-sm-4">
    <div class="form-group">
      <label for="sel1">Phone 1:</label>
      <select class="form-control" id="selectPhone1">
        <?php
        foreach ($phones as $phone) {
          $mobile = new Mobile($phone);
          $postID = $mobile->get(RelatedPostID);
          $title = $mobile->get(Title);
          $isSelected = false;
          if ($arePhonesLoaded) {
            $isSelected = (($phone1->get(RelatedPostID)));
          }
          $selected = ($isSelected ? "selected" : "");
          echo "<option $selected value='$postID'>$title</option>";
        }
        ?>
      </select>
    </div>
  </div>

  <div class="col-sm-4">
    <div class="form-group">
      <label for="sel1">Phone 2:</label>
      <select class="form-control" id="selectPhone2">
        <?php
        $count = 0;
        foreach ($phones as $phone) {
          $mobile = new Mobile($phone);
          $postID = $mobile->get(RelatedPostID);
          $title = $mobile->get(Title);

          if ($arePhonesLoaded) {
            $isSelected = ($phone2->get(RelatedPostID) == $postID);
          } else {
            $isSelected = ($count++ == 1);
          }

          $selected = ($isSelected ? "selected" : "");
          echo "<option $selected value='$postID'>$title</option>";
        }
        ?>
      </select>
    </div>
  </div>
  <div class="col-sm-4">
    <button onClick="xyz();" class="btn btn-sunny btn-lg">Compare</button>
  </div>
</div>
