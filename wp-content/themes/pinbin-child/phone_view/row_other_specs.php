<div class="row" style="margin-left:10%; margin-right: 10%; margin-top:5%">
  <div class="col-sm-12">
    <h4>Other features</h4>
    <table class="table-striped">
      <tbody>
        <?php
        $otherFeatures = explode("\n", $mobile->get(OtherFeatures));

        foreach ($otherFeatures as $feature) {
          echo "<tr><td>" . $feature . "</td></tr>";
        }
        ?>

      </tbody>
    </table>
  </div>
</div>
