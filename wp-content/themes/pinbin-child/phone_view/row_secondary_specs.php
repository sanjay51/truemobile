<!-- Start of secondary specs -->
<div class="row" style="margin-left:10%; margin-right: 10%; margin-top:5%">
  <div class="col-sm-6">
    <div class="panel panel-primary" >
      <div class="panel-body">

        <!-- Android -->
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('android', 'png') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Android</b></div>
              <div class="panel-body">Version <?= $mobile->get(AndroidVersion) ?></div>
            </div>
          </div>
        </div>

        <!-- SIM -->
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('sim') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>SIM</b></div>
              <div class="panel-body">
                1st: <?= $mobile->get(SIM1Type) . "(" . $mobile->get(SIM1Size) . ")" ?><br/>
                2nd: <?= $mobile->get(SIM2Type) . "(" . $mobile->get(SIM2Size) . ")" ?><br/>
                Dual standby
              </div>
            </div>
          </div>
        </div>


        <!-- Fingerprint sensor -->
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('fingerprint', 'png') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Fingerprint sensor</b></div>
              <div class="panel-body"><?= $mobile->get(FingerPrint) ?></div>
            </div>
          </div>
        </div>

        <!-- Charging -->
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('charger', 'png') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Charging</b></div>
              <div class="panel-body"><?= $mobile->get(ChargingType) ?></div>
            </div>
          </div>
        </div>

        <!-- Network -->
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('network', 'png') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Network</b></div>
              <div class="panel-body"><?= $mobile->get(Network) ?></div>
            </div>
          </div>
        </div>


        <!-- Weight -->
        <div class="row">
          <div class="col-sm-3"><img class="spec-icon" src="<?= getMetaImageLink('weight', 'png') ?>" /></div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading"><b>Weight</b></div>
              <div class="panel-body"><?= $mobile->get(WeightGrams) . " g" ?></div>
            </div>
          </div>
        </div>
        <!-- End of secondary specs -->
      </div>
    </div>

  </div>
  <div class="col-sm-6">
    <?= "<h4>" . $mobile->getSecondaryReview() . "</h4>" ?>
  </div>
</div>
