<?php
/**
* Links - page template
*/

include_once 'constants.php';
include_once 'utils.php';
include_once 'mobile.php';
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if ( has_post_thumbnail() ) { ?>
      <div class="pinbin-image"><?php the_post_thumbnail( 'detail-image' );  ?></div>
      <?php } ?>

      <div class="pinbin-copy">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>

        <!-- content starts -->
        <?php
          $phones = getAllPhones();
          foreach ($phones as $phone) {
            $mobile = new Mobile($phone);
            $link = get_post_permalink($mobile->get(RelatedPostID));
            $title = $mobile->get(Title);
            echo "Review: <a target='_blank' href='$link&review $title'>$title</a><br/>";
          }

          for ($i = 0; $i < count($phones); $i++) {
            for ($j = $i+1; $j < count($phones); $j++) {
              $mobile1 = new Mobile($phones[$i]);
              $title1 = $mobile1->get(Title);
              $pid1 = $mobile1->get(RelatedPostID);

              $mobile2 = new Mobile($phones[$j]);
              $title2 = $mobile2->get(Title);
              $pid2 = $mobile2->get(RelatedPostID);
              echo "Compare: <a target='_blank' href='compare-phones/?compare $title1 vs $title2&pid1=$pid1&pid2=$pid2'>$title1 vs $title2</a><br/>";
            }
          }
        ?>
        <!-- content ends -->

        <?php wp_link_pages(); ?>
        <?php comments_template(); ?>

      </div>

    </div>

  <?php endwhile; endif; ?>

  <?php get_footer(); ?>
