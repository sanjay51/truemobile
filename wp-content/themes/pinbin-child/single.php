<?php
/**
* Custom Single post template
*/

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

include 'constants.php';
include 'utils.php';
include 'mobile.php';
?>

<head>

  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
    margin: auto;
    width:100%
  }

  .fact {
    color:#33adea;
    font-size: 16px;
    font-weight: bolder;
    margin: 10px;
  }
  .spec-icon {
    width:100%;
  }
  </style>
</head>
<?php get_header(); ?>
<?php
$postID = get_the_ID();

$args = array(
  'fields'    => array(
    RelatedPostID   => $postID
  )
);

$submissions = Ninja_Forms()->subs()->get( $args );
$first_form_element = reset($submissions);
$mobile = new Mobile($first_form_element);
?>

<div class="container" style="background-color:white; box-shadow:0px 0px 10px black;">
  <div class="row">
    <?php include ('phone_view/col_main.php'); ?>
    <?php include ('phone_view/col_carousal.php'); ?>
  </div>

  <?php include ('phone_view/row_primary_specs.php'); ?>
  <?php include ('phone_view/row_secondary_specs.php'); ?>
  <?php include ('phone_view/row_other_specs.php'); ?>
  <?php include ('phone_view/row_reviews.php'); ?>

  <?php comments_template(); ?>
</div>

<?php get_footer(); ?>
