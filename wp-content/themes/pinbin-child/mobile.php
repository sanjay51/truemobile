<?php
include_once 'review-utils.php';

class Mobile {
  var $mobile;
  function __construct($ninja_mobile) {
    $this->mobile = $ninja_mobile;
  }

  function get($field) {
    return $this->mobile->get_field($field);
  }

  function getPhoneType() {
    $screenSizeInches = $this->get(ScreenSizeInches);
    if ($screenSizeInches > 5.5) {
      return "phablet";
    } else if ($screenSizeInches < 4.0) {
      return "small-screen";
    } else {
      return "standard-sized";
    }
  }

  function getDisplayType() {
    $ppi = $this->get(ScreenPpi);
    if ($ppi >= 700) {
      return "Ultra HD";
    } else if ($ppi >= 500) {
      return "QHD";
    } else if ($ppi >= 400) {
      return "Full HD";
    } else if ($ppi >= 300) {
      return "HD";
    } else {
      return "pixelated";
    }
  }

  function getExternalStorage() {
    $storage = $this->get(StorageExternalGB);
    if ($storage == "0") {
      return "Not supported";
    }

    return $storage . " GB";
  }

  function getRAMInGB() {
    return $this->get(RAMinMB)/1024;
  }

  function getPrimaryReview() {
    return getPrimaryReviewTemplate($this);
  }

  function getSecondaryReview() {
    return getSecondaryReviewTemplate($this);
  }
}
?>
