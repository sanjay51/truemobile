<?php
/**
* Single page template
*/

include_once 'constants.php';
include_once 'utils.php';
include_once 'mobile.php';
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if ( has_post_thumbnail() ) { ?>
			<div class="pinbin-image"><?php the_post_thumbnail( 'detail-image' );  ?></div>
			<?php } ?>

			<div class="pinbin-copy">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<!-- content starts here -->

				<?php
				$phones = getAllPhones();
				$mobiles = convertSubmissionsToMobiles($phones);
				$topTen = getTopTen($mobiles);

				for ($i = 0; $i < count($topTen); $i++) {
					$mobile = $topTen[$i];
					$link = get_post_permalink($mobile->get(RelatedPostID));
					$title = $mobile->get(Title);
					echo ($i+1) . ". <a href='$link'>$title</a><br/>";
				}

				?>



				<!-- content ends here -->
				<?php wp_link_pages(); ?>
				<?php comments_template(); ?>

			</div>

		</div>

	<?php endwhile; endif; ?>

	<?php get_footer(); ?>
