<?php
include_once "constants.php";

class Comparator {
  function compareMobile($mobile1, $mobile2) {
    return $this->calculateScore($mobile1) > $this->calculateScore($mobile2);
  }

  function calculateScore($mobile) {
    /*
    Should be minimum:
    - PriceINR
    - WeightGrams

    Should be maximum:
    - RAMinMB
    - StorageInternalGB
    - StorageExternalGB
    - ProcessorCores
    - ProcessorGHz
    - BatterymAH
    - CameraRearMP
    - CameraFrontMP
    - SIM count <<custom>>
    - fingerprint
    - ChargingType
    - AntutuScore

    Should be in range:
    - ScreenSizeInches

    Optimum score per factor: 100
    Minimum score per factor: 0
    No. of factors: 16
    Maximum overall score: 16 * 100 = 1600
    */

    $processorCoresScore = 10 * $this->getNormalizedValue($mobile->get(ProcessorCores), 32, 1);
    $processorGHzScore = 10 * $this->getNormalizedValue($mobile->get(ProcessorGHz), 5, 0.1);
    $priceScore = 5 * $this->calculateBasedOnMinThreshold($mobile->get(PriceINR), PriceINR, 5500);
    $weightScore = 2 * $this->calculateBasedOnMinThreshold($mobile->get(WeightGrams), WeightGrams, 80);
    $ramScore = 10 * $this->getNormalizedValue($mobile->get(RAMinMB), 32768, 100);
    $storageInternalScore = 10 * $this->getNormalizedValue($mobile->get(StorageInternalGB) * 1024, 256*1024, 100);
    $storageExternalScore = 5 * $this->getNormalizedValue($mobile->get(StorageExternalGB) * 1024, 256*1024, 0);
    $batteryScore = 10 * $this->getNormalizedValue($mobile->get(BatterymAH), 10000, 100);
    $cameraRearScore = 5 * $this->getNormalizedValue($mobile->get(CameraRearMP), 64, 0);
    $cameraFrontScore = 3 * $this->getNormalizedValue($mobile->get(CameraFrontMP), 64, 0);
    $simScore = 8 * $this->calculateBasedOnAvailability($mobile->get(SIM2Type));
    $fingerprintScore = 4 * $this->calculateBasedOnAvailability($mobile->get(FingerPrint));
    $chargingScore = 4 * $this->calculateBasedOnAvailability($mobile->get(FastCharge));
    $antutuScore = 8 * $this->getNormalizedValue($mobile->get(AntutuScore), 1000000, 100);

    $score = $processorCoresScore + $processorGHzScore
    + $priceScore + $weightScore + $ramScore
    + $storageInternalScore + $storageExternalScore
    + $batteryScore + $cameraRearScore + $cameraFrontScore
    + $simScore + $fingerprintScore + $chargingScore + $antutuScore;

    return $score;
  }

  function calculateBasedOnMinThreshold($value, $type, $minThreshold) {
    if ($value <= $minThreshold) {
      return 100;
    }

    $ret = $this->getNormalizedValue($value, 0, 15000, $type);

    return $ret;
  }

  function getNormalizedValue($rawValue, $max, $min, $type = 0) {
    if ($type == PriceINR || $type == WeightGrams) {
      $rawValue = $this->invertValue($rawValue, $max, $min);
    }

    return 10 * ($rawValue - $min) / ($max - $min); //returns in range 0 to 10
  }

  function invertValue($value, $max, $min) {
    return $max + $min - $value;
  }

  function calculateBasedOnAvailability($value) {
    //100 if dual SIM
    if ($value == "") {
      return 0;
    }

    return 100;
  }
}
?>
