<?php
/**
 * The template for displaying the footer.
 */

?>

<?php if ( is_active_sidebar( 'pinbin_footer')) { ?>
   <div id="footer-area">
			<?php dynamic_sidebar( 'pinbin_footer' ); ?>
        </div><!-- // footer area with widgets -->
<?php }  ?>
<footer class="site-footer">
	 <div id="copyright">
	 	<?php _e( 'Copyright', 'pinbin' ) ?> <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> | All rights reserved.
	</div><!-- // copyright -->
</footer>
</div><!-- // close wrap div -->

<?php wp_footer(); ?>

</body>
</html>
